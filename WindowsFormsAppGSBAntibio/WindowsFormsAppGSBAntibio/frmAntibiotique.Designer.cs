﻿namespace WindowsFormsAppGSBAntibio
{
    partial class frmAntibiotique
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstAntibio = new System.Windows.Forms.ListBox();
            this.lblLibelleG = new System.Windows.Forms.Label();
            this.lblUnite = new System.Windows.Forms.Label();
            this.lblNbreParJour = new System.Windows.Forms.Label();
            this.lblCateg = new System.Windows.Forms.Label();
            this.lblDose = new System.Windows.Forms.Label();
            this.lblDoseTotale = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lstAntibio
            // 
            this.lstAntibio.FormattingEnabled = true;
            this.lstAntibio.Location = new System.Drawing.Point(49, 44);
            this.lstAntibio.Name = "lstAntibio";
            this.lstAntibio.Size = new System.Drawing.Size(189, 173);
            this.lstAntibio.TabIndex = 0;
            this.lstAntibio.SelectedIndexChanged += new System.EventHandler(this.lstAntibio_SelectedIndexChanged);
            // 
            // lblLibelleG
            // 
            this.lblLibelleG.AutoSize = true;
            this.lblLibelleG.Location = new System.Drawing.Point(203, 250);
            this.lblLibelleG.Name = "lblLibelleG";
            this.lblLibelleG.Size = new System.Drawing.Size(87, 13);
            this.lblLibelleG.TabIndex = 1;
            this.lblLibelleG.Text = "Libelle générique";
            // 
            // lblUnite
            // 
            this.lblUnite.AutoSize = true;
            this.lblUnite.Location = new System.Drawing.Point(203, 274);
            this.lblUnite.Name = "lblUnite";
            this.lblUnite.Size = new System.Drawing.Size(32, 13);
            this.lblUnite.TabIndex = 2;
            this.lblUnite.Text = "Unité";
            // 
            // lblNbreParJour
            // 
            this.lblNbreParJour.AutoSize = true;
            this.lblNbreParJour.Location = new System.Drawing.Point(203, 297);
            this.lblNbreParJour.Name = "lblNbreParJour";
            this.lblNbreParJour.Size = new System.Drawing.Size(82, 13);
            this.lblNbreParJour.TabIndex = 3;
            this.lblNbreParJour.Text = "Nombre par jour";
            // 
            // lblCateg
            // 
            this.lblCateg.AutoSize = true;
            this.lblCateg.Location = new System.Drawing.Point(203, 321);
            this.lblCateg.Name = "lblCateg";
            this.lblCateg.Size = new System.Drawing.Size(52, 13);
            this.lblCateg.TabIndex = 4;
            this.lblCateg.Text = "Catégorie";
            // 
            // lblDose
            // 
            this.lblDose.AutoSize = true;
            this.lblDose.Location = new System.Drawing.Point(203, 345);
            this.lblDose.Name = "lblDose";
            this.lblDose.Size = new System.Drawing.Size(32, 13);
            this.lblDose.TabIndex = 5;
            this.lblDose.Text = "Dose";
            // 
            // lblDoseTotale
            // 
            this.lblDoseTotale.AutoSize = true;
            this.lblDoseTotale.Location = new System.Drawing.Point(203, 371);
            this.lblDoseTotale.Name = "lblDoseTotale";
            this.lblDoseTotale.Size = new System.Drawing.Size(99, 13);
            this.lblDoseTotale.TabIndex = 6;
            this.lblDoseTotale.Text = "Dose totale par jour";
            // 
            // frmAntibiotique
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 423);
            this.Controls.Add(this.lblDoseTotale);
            this.Controls.Add(this.lblDose);
            this.Controls.Add(this.lblCateg);
            this.Controls.Add(this.lblNbreParJour);
            this.Controls.Add(this.lblUnite);
            this.Controls.Add(this.lblLibelleG);
            this.Controls.Add(this.lstAntibio);
            this.Name = "frmAntibiotique";
            this.Text = "frmAntibiotique";
            this.Load += new System.EventHandler(this.frmAntibiotique_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstAntibio;
        private System.Windows.Forms.Label lblLibelleG;
        private System.Windows.Forms.Label lblUnite;
        private System.Windows.Forms.Label lblNbreParJour;
        private System.Windows.Forms.Label lblCateg;
        private System.Windows.Forms.Label lblDose;
        private System.Windows.Forms.Label lblDoseTotale;
    }
}