﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsAppGSBAntibio.mesClasses;


namespace WindowsFormsAppGSBAntibio
{
    public partial class frmAntibiotique : Form
    {
        List<Antibiotique> lesAntibiotiques; 
        public frmAntibiotique()
        {
            InitializeComponent();
        }

        private void frmAntibiotique_Load(object sender, EventArgs e)
        {
            DataAntibio.initialiser();
            Categorie c = null;
            lesAntibiotiques = DataAntibio.getLesAntibiotiquesParCateg(c);
            foreach (Antibiotique a in lesAntibiotiques)
            {
                lstAntibio.Items.Add(a.getLibelle().ToUpper());
            }

        }

        private void lstAntibio_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
