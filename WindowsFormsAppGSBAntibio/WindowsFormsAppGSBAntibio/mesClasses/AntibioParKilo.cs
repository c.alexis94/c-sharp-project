﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsAppGSBAntibio.mesClasses
{
    public class AntibioParKilo : Antibiotique
    {
    private float doseKilo;
    public AntibioParKilo(String pLibelle, String pLibelleG, String pUnite, int pNombreParJour, Categorie pCategorie, float pDoseKilo):base(pLibelle, pLibelleG, pUnite, pNombreParJour, pCategorie)
    {        
        this.doseKilo = pDoseKilo;
    }
    public float getDoseKilo()
    {
        return this.doseKilo;
    }
}

}
